﻿namespace William
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.txtPregunta = new System.Windows.Forms.TextBox();
            this.txtRespuesta1 = new System.Windows.Forms.TextBox();
            this.txtRespuesta2 = new System.Windows.Forms.TextBox();
            this.txtRespuesta3 = new System.Windows.Forms.TextBox();
            this.txtRespuesta4 = new System.Windows.Forms.TextBox();
            this.lblNumero = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.txtRespuesta7 = new System.Windows.Forms.TextBox();
            this.txtRespuesta6 = new System.Windows.Forms.TextBox();
            this.txtRespuesta5 = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.txtRespuesta8 = new System.Windows.Forms.TextBox();
            this.txtRespuesta9 = new System.Windows.Forms.TextBox();
            this.button3 = new System.Windows.Forms.Button();
            this.trkIndex = new System.Windows.Forms.TrackBar();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.trkIndex)).BeginInit();
            this.SuspendLayout();
            // 
            // txtPregunta
            // 
            this.txtPregunta.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPregunta.Location = new System.Drawing.Point(12, 12);
            this.txtPregunta.Multiline = true;
            this.txtPregunta.Name = "txtPregunta";
            this.txtPregunta.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtPregunta.Size = new System.Drawing.Size(883, 254);
            this.txtPregunta.TabIndex = 0;
            this.txtPregunta.Text = "Acá van las preguntas";
            this.txtPregunta.WordWrap = false;
            this.txtPregunta.TextChanged += new System.EventHandler(this.txtPregunta_TextChanged);
            // 
            // txtRespuesta1
            // 
            this.txtRespuesta1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtRespuesta1.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRespuesta1.Location = new System.Drawing.Point(12, 272);
            this.txtRespuesta1.Name = "txtRespuesta1";
            this.txtRespuesta1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtRespuesta1.Size = new System.Drawing.Size(883, 23);
            this.txtRespuesta1.TabIndex = 1;
            this.txtRespuesta1.TextChanged += new System.EventHandler(this.txtRespuesta1_TextChanged);
            // 
            // txtRespuesta2
            // 
            this.txtRespuesta2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.txtRespuesta2.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRespuesta2.Location = new System.Drawing.Point(12, 301);
            this.txtRespuesta2.Name = "txtRespuesta2";
            this.txtRespuesta2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtRespuesta2.Size = new System.Drawing.Size(883, 23);
            this.txtRespuesta2.TabIndex = 2;
            this.txtRespuesta2.TextChanged += new System.EventHandler(this.txtRespuesta2_TextChanged);
            // 
            // txtRespuesta3
            // 
            this.txtRespuesta3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.txtRespuesta3.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRespuesta3.Location = new System.Drawing.Point(12, 330);
            this.txtRespuesta3.Name = "txtRespuesta3";
            this.txtRespuesta3.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtRespuesta3.Size = new System.Drawing.Size(883, 23);
            this.txtRespuesta3.TabIndex = 3;
            this.txtRespuesta3.TextChanged += new System.EventHandler(this.txtRespuesta3_TextChanged);
            // 
            // txtRespuesta4
            // 
            this.txtRespuesta4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.txtRespuesta4.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRespuesta4.Location = new System.Drawing.Point(12, 359);
            this.txtRespuesta4.Name = "txtRespuesta4";
            this.txtRespuesta4.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtRespuesta4.Size = new System.Drawing.Size(883, 23);
            this.txtRespuesta4.TabIndex = 4;
            this.txtRespuesta4.TextChanged += new System.EventHandler(this.txtRespuesta4_TextChanged);
            // 
            // lblNumero
            // 
            this.lblNumero.AutoSize = true;
            this.lblNumero.Location = new System.Drawing.Point(12, 533);
            this.lblNumero.Name = "lblNumero";
            this.lblNumero.Size = new System.Drawing.Size(100, 13);
            this.lblNumero.TabIndex = 5;
            this.lblNumero.Text = "Pregunta YYY/XXX";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(814, 533);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(81, 34);
            this.button1.TabIndex = 6;
            this.button1.Text = "Continue >>";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // txtRespuesta7
            // 
            this.txtRespuesta7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.txtRespuesta7.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRespuesta7.Location = new System.Drawing.Point(12, 446);
            this.txtRespuesta7.Name = "txtRespuesta7";
            this.txtRespuesta7.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtRespuesta7.Size = new System.Drawing.Size(883, 23);
            this.txtRespuesta7.TabIndex = 9;
            this.txtRespuesta7.TextChanged += new System.EventHandler(this.txtRespuesta7_TextChanged);
            // 
            // txtRespuesta6
            // 
            this.txtRespuesta6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.txtRespuesta6.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRespuesta6.Location = new System.Drawing.Point(12, 417);
            this.txtRespuesta6.Name = "txtRespuesta6";
            this.txtRespuesta6.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtRespuesta6.Size = new System.Drawing.Size(883, 23);
            this.txtRespuesta6.TabIndex = 8;
            this.txtRespuesta6.TextChanged += new System.EventHandler(this.txtRespuesta6_TextChanged);
            // 
            // txtRespuesta5
            // 
            this.txtRespuesta5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.txtRespuesta5.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRespuesta5.Location = new System.Drawing.Point(12, 388);
            this.txtRespuesta5.Name = "txtRespuesta5";
            this.txtRespuesta5.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtRespuesta5.Size = new System.Drawing.Size(883, 23);
            this.txtRespuesta5.TabIndex = 7;
            this.txtRespuesta5.TextChanged += new System.EventHandler(this.txtRespuesta5_TextChanged);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(733, 533);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 34);
            this.button2.TabIndex = 10;
            this.button2.Text = "<< Previous";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // txtRespuesta8
            // 
            this.txtRespuesta8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.txtRespuesta8.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRespuesta8.Location = new System.Drawing.Point(12, 475);
            this.txtRespuesta8.Name = "txtRespuesta8";
            this.txtRespuesta8.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtRespuesta8.Size = new System.Drawing.Size(883, 23);
            this.txtRespuesta8.TabIndex = 11;
            this.txtRespuesta8.TextChanged += new System.EventHandler(this.txtRespuesta8_TextChanged);
            // 
            // txtRespuesta9
            // 
            this.txtRespuesta9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.txtRespuesta9.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRespuesta9.Location = new System.Drawing.Point(12, 504);
            this.txtRespuesta9.Name = "txtRespuesta9";
            this.txtRespuesta9.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtRespuesta9.Size = new System.Drawing.Size(883, 23);
            this.txtRespuesta9.TabIndex = 12;
            this.txtRespuesta9.TextChanged += new System.EventHandler(this.txtRespuesta9_TextChanged);
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.button3.Location = new System.Drawing.Point(664, 533);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(63, 34);
            this.button3.TabIndex = 13;
            this.button3.Text = "Save";
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // trkIndex
            // 
            this.trkIndex.Location = new System.Drawing.Point(118, 533);
            this.trkIndex.Name = "trkIndex";
            this.trkIndex.Size = new System.Drawing.Size(378, 45);
            this.trkIndex.TabIndex = 14;
            this.trkIndex.ValueChanged += new System.EventHandler(this.trkIndex_ValueChanged);
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.button4.Location = new System.Drawing.Point(570, 533);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(88, 34);
            this.button4.TabIndex = 15;
            this.button4.Text = "Generate SQL";
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.button5.Location = new System.Drawing.Point(502, 533);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(62, 34);
            this.button5.TabIndex = 16;
            this.button5.Text = "List";
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(907, 581);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.trkIndex);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.txtRespuesta9);
            this.Controls.Add(this.txtRespuesta8);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.txtRespuesta7);
            this.Controls.Add(this.txtRespuesta6);
            this.Controls.Add(this.txtRespuesta5);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.lblNumero);
            this.Controls.Add(this.txtRespuesta4);
            this.Controls.Add(this.txtRespuesta3);
            this.Controls.Add(this.txtRespuesta2);
            this.Controls.Add(this.txtRespuesta1);
            this.Controls.Add(this.txtPregunta);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmMain";
            this.Text = "William";
            this.Load += new System.EventHandler(this.frmMain_Load);
            ((System.ComponentModel.ISupportInitialize)(this.trkIndex)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtPregunta;
        private System.Windows.Forms.TextBox txtRespuesta1;
        private System.Windows.Forms.TextBox txtRespuesta2;
        private System.Windows.Forms.TextBox txtRespuesta3;
        private System.Windows.Forms.TextBox txtRespuesta4;
        private System.Windows.Forms.Label lblNumero;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox txtRespuesta7;
        private System.Windows.Forms.TextBox txtRespuesta6;
        private System.Windows.Forms.TextBox txtRespuesta5;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox txtRespuesta8;
        private System.Windows.Forms.TextBox txtRespuesta9;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.TrackBar trkIndex;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
    }
}

