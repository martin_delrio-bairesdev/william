﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows;

namespace William
{
    public partial class frmMain : Form
    {
        string sourcePreguntas;
        List<tuplaPreguntaRespuesta> preguntasYRespuestas;
        public static int maxRespuestas = 9; //Maxima cantidad de respuestas
        int preguntaActual = 0;
        string filepath;

        public frmMain()
        {
            InitializeComponent();
            preguntasYRespuestas = new List<tuplaPreguntaRespuesta>();
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            System.Windows.Forms.OpenFileDialog openFileDialog;
            openFileDialog = new System.Windows.Forms.OpenFileDialog();
            openFileDialog.Title = "Please locate ListaIngles.txt";
            DialogResult dr = openFileDialog.ShowDialog();
            if (dr == System.Windows.Forms.DialogResult.OK)
            {
                filepath = openFileDialog.FileName;
                sourcePreguntas = File.ReadAllText(filepath);
                string[] lineas = sourcePreguntas.Split('\n');
                tuplaPreguntaRespuesta currentPreguntaRespuesta = new tuplaPreguntaRespuesta();
                bool parseandoPregunta = false;
                int parseandoRespuesta = 0;
                foreach (string linea in lineas)
                {
                    string[] tokens = linea.Split(' ');
                    if (tokens.Length == 0) continue;
                    if (tokens[0].Trim().Length < 1) continue;
                    if (tokens[0] == "#$#$@")
                    {
                        switch (tokens[1].Trim())
                        {
                            case "INICIO-PREGUNTA":
                                if (currentPreguntaRespuesta.pregunta != "")
                                {
                                    preguntasYRespuestas.Add(currentPreguntaRespuesta);
                                }
                                currentPreguntaRespuesta = new tuplaPreguntaRespuesta();
                                currentPreguntaRespuesta.respuestas = new string[maxRespuestas];
                                for(int i = 0; i < maxRespuestas; ++i)
                                {
                                    currentPreguntaRespuesta.respuestas[i] = "";
                                }
                                currentPreguntaRespuesta.idRespuesta = new int[maxRespuestas];
                                currentPreguntaRespuesta.pregunta = "";
                                currentPreguntaRespuesta.idPregunta = int.Parse(tokens[2]);
                                currentPreguntaRespuesta.correcta = 0;
                                parseandoPregunta = true;
                                parseandoRespuesta = 0;
                                break;
                            case "FIN-PREGUNTA":
                                parseandoPregunta = false;
                                break;
                            case "INICIO-RESPUESTA":
                                currentPreguntaRespuesta.idRespuesta[parseandoRespuesta] = int.Parse(tokens[2]);
                                break;
                            case "CORRECTA?":
                                if (tokens[2].Trim() == "1")
                                    currentPreguntaRespuesta.correcta = parseandoRespuesta;
                                break;
                            case "FIN-RESPUESTA":
                                ++parseandoRespuesta;
                                break;
                            case "ACTUAL":
                                preguntaActual = int.Parse(tokens[2]);
                                break;
                        }
                    }
                    else
                    {
                        if (parseandoPregunta)
                        {
                            currentPreguntaRespuesta.pregunta += linea + "\r\n";
                        }
                        else
                        {
                            currentPreguntaRespuesta.respuestas[parseandoRespuesta] += linea + "\r\n";
                        }
                    }
                }
                preguntasYRespuestas.Add(currentPreguntaRespuesta);
                trkIndex.Minimum = 0;
                trkIndex.Maximum = preguntasYRespuestas.Count - 1;
                cargarPregunta(preguntaActual);
            }
            else
            {
                Application.Exit();
            }
        }

        private void cargarPregunta(int index)
        {
            trkIndex.Value = index;
            lblNumero.Text = (index+1) + "/" + preguntasYRespuestas.Count;
            txtPregunta.Text = preguntasYRespuestas[index].pregunta;
            // Respuesta 1
            if(preguntasYRespuestas[index].respuestas[0] != "")
            {
                txtRespuesta1.Enabled = true;
                if (preguntasYRespuestas[index].correcta == 0)
                    txtRespuesta1.BackColor = Color.LightGreen;
                else
                    txtRespuesta1.BackColor = Color.Coral;
                txtRespuesta1.Text = preguntasYRespuestas[index].respuestas[0];
            }
            else
            {
                txtRespuesta1.Enabled = false;
                txtRespuesta1.BackColor = Color.DimGray;
            }
            // Respuesta 2
            if (preguntasYRespuestas[index].respuestas[1] != "")
            {
                txtRespuesta2.Enabled = true;
                if (preguntasYRespuestas[index].correcta == 1)
                    txtRespuesta2.BackColor = Color.LightGreen;
                else
                    txtRespuesta2.BackColor = Color.Coral;
                txtRespuesta2.Text = preguntasYRespuestas[index].respuestas[1];
            }
            else
            {
                txtRespuesta2.Enabled = false;
                txtRespuesta2.BackColor = Color.DimGray;
            }
            // Respuesta 3
            if (preguntasYRespuestas[index].respuestas[2] != "")
            {
                txtRespuesta3.Enabled = true;
                if (preguntasYRespuestas[index].correcta == 2)
                    txtRespuesta3.BackColor = Color.LightGreen;
                else
                    txtRespuesta3.BackColor = Color.Coral;
                txtRespuesta3.Text = preguntasYRespuestas[index].respuestas[2];
            }
            else
            {
                txtRespuesta3.Enabled = false;
                txtRespuesta3.BackColor = Color.DimGray;
            }
            // Respuesta 4
            if (preguntasYRespuestas[index].respuestas[3] != "")
            {
                txtRespuesta4.Enabled = true;
                if (preguntasYRespuestas[index].correcta == 3)
                    txtRespuesta4.BackColor = Color.LightGreen;
                else
                    txtRespuesta4.BackColor = Color.Coral;
                txtRespuesta4.Text = preguntasYRespuestas[index].respuestas[3];
            }
            else
            {
                txtRespuesta4.Enabled = false;
                txtRespuesta4.BackColor = Color.DimGray;
            }
            // Respuesta 5
            if (preguntasYRespuestas[index].respuestas[4] != "")
            {
                txtRespuesta5.Enabled = true;
                if (preguntasYRespuestas[index].correcta == 4)
                    txtRespuesta5.BackColor = Color.LightGreen;
                else
                    txtRespuesta5.BackColor = Color.Coral;
                txtRespuesta5.Text = preguntasYRespuestas[index].respuestas[4];
            }
            else
            {
                txtRespuesta5.Enabled = false;
                txtRespuesta5.BackColor = Color.DimGray;
            }
            // Respuesta 6
            if (preguntasYRespuestas[index].respuestas[5] != "")
            {
                txtRespuesta6.Enabled = true;
                if (preguntasYRespuestas[index].correcta == 5)
                    txtRespuesta6.BackColor = Color.LightGreen;
                else
                    txtRespuesta6.BackColor = Color.Coral;
                txtRespuesta6.Text = preguntasYRespuestas[index].respuestas[5];
            }
            else
            {
                txtRespuesta6.Enabled = false;
                txtRespuesta6.BackColor = Color.DimGray;
            }
            // Respuesta 7
            if (preguntasYRespuestas[index].respuestas[6] != "")
            {
                txtRespuesta7.Enabled = true;
                if (preguntasYRespuestas[index].correcta == 6)
                    txtRespuesta7.BackColor = Color.LightGreen;
                else
                    txtRespuesta7.BackColor = Color.Coral;
                txtRespuesta7.Text = preguntasYRespuestas[index].respuestas[6];
            }
            else
            {
                txtRespuesta7.Enabled = false;
                txtRespuesta7.BackColor = Color.DimGray;
            }
            // Respuesta 8
            if(preguntasYRespuestas[index].respuestas[7] != "")
            {
                txtRespuesta8.Enabled = true;
                if (preguntasYRespuestas[index].correcta == 7)
                    txtRespuesta8.BackColor = Color.LightGreen;
                else
                    txtRespuesta8.BackColor = Color.Coral;
                txtRespuesta8.Text = preguntasYRespuestas[index].respuestas[7];
            }
            else
            {
                txtRespuesta8.Enabled = false;
                txtRespuesta8.BackColor = Color.DimGray;
            }
            // Respuesta 9
            if (preguntasYRespuestas[index].respuestas[8] != "")
            {
                txtRespuesta9.Enabled = true;
                if (preguntasYRespuestas[index].correcta == 8)
                    txtRespuesta9.BackColor = Color.LightGreen;
                else
                    txtRespuesta9.BackColor = Color.Coral;
                txtRespuesta9.Text = preguntasYRespuestas[index].respuestas[8];
            }
            else
            {
                txtRespuesta9.Enabled = false;
                txtRespuesta9.BackColor = Color.DimGray;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if(preguntaActual < trkIndex.Maximum)
                cargarPregunta(++preguntaActual);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (preguntaActual > 0)
                cargarPregunta(--preguntaActual);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            File.WriteAllText(filepath, "");
            using (StreamWriter outputFile = new StreamWriter(filepath))
            {
                foreach(tuplaPreguntaRespuesta preg in preguntasYRespuestas)
                {
                    outputFile.WriteLine("#$#$@ INICIO-PREGUNTA " + preg.idPregunta);
                    outputFile.WriteLine(preg.pregunta.Trim());
                    outputFile.WriteLine("#$#$@ FIN-PREGUNTA");
                    for(int i = 0; i < maxRespuestas; ++i)
                    {
                        if(preg.respuestas[i] != "")
                        {
                            outputFile.WriteLine("#$#$@ INICIO-RESPUESTA " + preg.idRespuesta[i]);
                            outputFile.WriteLine(preg.respuestas[i].Trim());
                            outputFile.WriteLine("#$#$@ CORRECTA? " + (preg.correcta == i ? "1" : "0"));
                            outputFile.WriteLine("#$#$@ FIN-RESPUESTA");
                        }
                    }
                }
                outputFile.WriteLine("#$#$@ ACTUAL " + preguntaActual);
            }
        }

        private void txtRespuesta1_TextChanged(object sender, EventArgs e)
        {
            preguntasYRespuestas[preguntaActual].respuestas[0] = txtRespuesta1.Text;
        }

        private void trkIndex_ValueChanged(object sender, EventArgs e)
        {
            preguntaActual = trkIndex.Value;
            Console.WriteLine(preguntaActual);
            cargarPregunta(preguntaActual);
        }

        private void txtRespuesta2_TextChanged(object sender, EventArgs e)
        {
            preguntasYRespuestas[preguntaActual].respuestas[1] = txtRespuesta2.Text;
        }

        private void txtRespuesta3_TextChanged(object sender, EventArgs e)
        {
            preguntasYRespuestas[preguntaActual].respuestas[2] = txtRespuesta3.Text;
        }

        private void txtRespuesta4_TextChanged(object sender, EventArgs e)
        {
            preguntasYRespuestas[preguntaActual].respuestas[3] = txtRespuesta4.Text;
        }

        private void txtRespuesta5_TextChanged(object sender, EventArgs e)
        {
            preguntasYRespuestas[preguntaActual].respuestas[4] = txtRespuesta5.Text;
        }

        private void txtRespuesta6_TextChanged(object sender, EventArgs e)
        {
            preguntasYRespuestas[preguntaActual].respuestas[5] = txtRespuesta6.Text;
        }

        private void txtRespuesta7_TextChanged(object sender, EventArgs e)
        {
            preguntasYRespuestas[preguntaActual].respuestas[6] = txtRespuesta7.Text;
        }

        private void txtRespuesta8_TextChanged(object sender, EventArgs e)
        {
            preguntasYRespuestas[preguntaActual].respuestas[7] = txtRespuesta8.Text;
        }

        private void txtRespuesta9_TextChanged(object sender, EventArgs e)
        {
            preguntasYRespuestas[preguntaActual].respuestas[8] = txtRespuesta9.Text;
        }

        private void txtPregunta_TextChanged(object sender, EventArgs e)
        {
            preguntasYRespuestas[preguntaActual].pregunta = txtPregunta.Text;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            string sql = "";
            foreach (tuplaPreguntaRespuesta preg in preguntasYRespuestas)
            {
                sql += "update preguntas set Pregunta = '" + preg.pregunta.Trim().Replace("'", "''") + "' where idPregunta = " + preg.idPregunta + "\r\n";
                for (int i = 0; i < maxRespuestas; ++i)
                {
                    if (preg.respuestas[i] != "")
                    {
                        sql += "update respuestasxpregunta set Descripcion = '" + preg.respuestas[i].Trim().Replace("'", "''") + "' where idRespuestaXPregunta = " + preg.idRespuesta[i] + "\r\n";
                    }
                }
            }
            Clipboard.SetText(sql);
            MessageBox.Show("Copied to the clipboard.");
        }

        private void button5_Click(object sender, EventArgs e)
        {
            string list = "";
            foreach (tuplaPreguntaRespuesta preg in preguntasYRespuestas)
            {
                list += preg.pregunta.Trim() + "\r\n";
                for (int i = 0; i < maxRespuestas; ++i)
                {
                    if (preg.respuestas[i] != "")
                    {
                        list += (i+1) + ") " + preg.respuestas[i].Trim() + (preg.correcta == i ? " (correcta) " : "") + "\r\n";
                    }
                }
                list += "\r\n";
            }
            Clipboard.SetText(list);
            MessageBox.Show("Copied to the clipboard.");
        }
    }

    public class tuplaPreguntaRespuesta
    {
        public int idPregunta;
        public string pregunta;
        public int[] idRespuesta;
        public int correcta;
        public string[] respuestas;
        public tuplaPreguntaRespuesta()
        {
            this.respuestas = new string[frmMain.maxRespuestas];
            this.idRespuesta = new int[frmMain.maxRespuestas];
            this.pregunta = "";
            this.idPregunta = 0;
            this.correcta = 0;
        }
    }
}
