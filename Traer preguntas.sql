﻿use wynnom_soporte
go

set nocount on

DECLARE @cant_preguntas BIGINT, @i BIGINT, @index bigint, @pregunta varchar(max), @correcto bit
DECLARE @tabla TABLE (
	idPregunta bigint,
	pregunta varchar(max)
)
DECLARE @tabla_res TABLE (
	idRespuestaXPregunta bigint,
	Descripcion varchar(max),
	Correcta bit
)
DECLARE @examenes_ingles TABLE (
	idExamen bigint
)

insert into @examenes_ingles select idexamen from examenes where idExamen = 25--idIdioma = 2 
insert into @tabla select idPregunta, Pregunta from vi_preguntasxexamen where idExamen in (select * from @examenes_ingles)

select @cant_preguntas = count(*) from @tabla

print concat('#$#$@ COMENTARIO Hay ', @cant_preguntas, ' preguntas en inglés.');
set @i = 0
while @i < @cant_preguntas
begin
	-- Printeo pregunta
	select top 1 @index = idPregunta, @pregunta = pregunta from @tabla
	print concat('#$#$@ INICIO-PREGUNTA ', @index)
	print @pregunta
	print '#$#$@ FIN-PREGUNTA'
	delete top(1) from @tabla
	set @i = @i + 1

	-- Traigo respuestas
	insert into @tabla_res select idRespuestaXPregunta, Descripcion, RespuestaCorrecta from respuestasxpregunta where idPregunta = @index
	while (select count(*) from @tabla_res) > 0
	begin
		select top 1 @index = idRespuestaXPregunta, @pregunta = Descripcion, @correcto = Correcta from @tabla_res
		print concat('#$#$@ INICIO-RESPUESTA ', @index)
		print @pregunta
		print concat('#$#$@ CORRECTA? ', @correcto)
		print '#$#$@ FIN-RESPUESTA'
		delete top(1) from @tabla_res
	end
end