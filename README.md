# William
William es una herramienta que permite descargar las preguntas y las 
respuestas de los tests y editarlas de forma cómoda y sencilla. Luego 
permite exportarlas a un query SQL a correr en la DB o a una lista de 
preguntas y respuestas que se puede mostrar a un humano.

![screenshot.png](screenshot.png)

## Cómo usar
Correr el archivo sql incluído para bajar las preguntas del test que se 
desee (modificarlo acordemente). Guardar el output del SQL en un archivo 
y correr William. Abrir el archivo. El campo de texto blanco muestra la 
pregunta tal como está guardada en el archivo ahora mismo. Los campos 
rojos y verdes de abajo muestran las respuestas, en verde la correcta. 
Todos los campos de texto son editables.

 * El botón *Save* permite guardar en este archivo nuestros cambios.
 * El botón *Generate SQL* copia al portapapeles un query SQL para 
guardar los cambios en la DB.
 * El botón *List* copia al portapapeles una lista de preguntas y 
respuestas human-readable.
